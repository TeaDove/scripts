# Archive
https://github.com/TeaDove/scripts

# Всякие полезные финтифлюшки
В самом репозитории находятся quickstarts(аля Hello World из мира фреймворков) для разных фреймворков, библиотек, а также шаблоны, i.e. Docker, ansible, supervisor и тд.
Используйте код в данном репозитории на свой страх и риск, я его использую как лекционную тетрадь.

# Content
- [Linux](https://gitlab.com/TeaDove/scripts/-/tree/master/linux#lifehacks)
- [Python](https://gitlab.com/TeaDove/scripts/-/tree/master/python#lifehacks)


# Extra
В .gitignore находятся:
- питоновские файлы(\_\_pycache\_\_ и тд)
- вимовские временные файлы
- supervisor логи
